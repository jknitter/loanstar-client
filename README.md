# README #

NodeJs need to be installed first. https://nodejs.org/ 
The app was developed on version 5.3, but other recent, more stable versions should work too.

### Install (this works on windows, not sure about anything else)
* Download and extract the client-server to a location of your choosing.
* Open a Command Prompt/Console Window and cd to the directory where the files were extracted.
* Type: npm install 
* Hit enter to start the install

### Run the Client-Server App
* Once the installer has run, type: npm index
* Hit enter to start the application.

The client-server will ask for a user name and password, which is the same as the same as the one used on the website ( currently: http://loanstar.jacobknitter.com ).

Note: the password will not be visible as it is entered

### Changing the PORT the Client-Server uses
* To use a different port, port 5000 for example, in the current console window, before running the client-server, type the following and hit enter: set PORT=5000 
* A more permanent option would be to change the port on the website ( currently: http://loanstar.jacobknitter.com/settings). Once the new port number is saved, if your Client-Server is running it will need to be restarted before the changes take affect.

The application will run on PORT 4000 by default