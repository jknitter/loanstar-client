'use strict';

define([
	'http', 'querystring', 'cli-color',
	'app/enums/socketMessageEnum'
],
function(http, querystring, clc, smEnum){
	var failedMsg = 'Client log in failed. ';

	return {
		logIntoServer: function(app, config, callback){
			console.log('');
			console.log(clc.yellow('Attempting to LOGIN as ' + config.userName));

			app.socket.emit(smEnum.User_csLogin, config, function(err, user){
				if(err){
					console.log(clc.redBright(err));
					callback(err);
				}
				else if(!user){ 
					console.log(clc.redBright('User Not Found'));
				}
				else {
					console.log(clc.green('Logged in as ' + user.userName +' at ' + new Date().toUTCString()));
					callback(err, user);			
				}
			});
		}
	}
});


