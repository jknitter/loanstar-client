'use strict';

define([
	'fs',
	'underscore',
	'socket.io-stream',

	'utils/js/crypto',
	'utils/file/video',

	'app/enums/socketMessageEnum'
],
function(fs, _ , ss, CryptoUtil, VideoUtil, smEnum){

	return new Object({
		//	req.params = collectionName, userId, uuid
		requestFileForPlayback: function(app, data){
			// find content collection, since we dont send file paths to the server
			var collectionWithFile = _.findWhere(app.contentCollections, {name: data.collectionName});
			if(!collectionWithFile){
				var msg = 'no collectionWithFile in videoController.loacateContentItem';
				console.error(msg);
				//return res.status(400).send({ message: msg });
			}
			
			// find file in the collection
			var contentItem = app.contentController.loacateContentItem(collectionWithFile, data.uuid);
			if(!contentItem) { 
				var msg = 'fileStreamRequest for ' + data.uuid + '(' + data.collectionName + ') not found in my contentCollections'
				console.log(msg); 
				return;
			}

			// read the file's stats
			var path = CryptoUtil.decrypt(contentItem.uuid);
			console.log("requested file: " + path);
			var stat = fs.statSync( path );

			if(!stat.isFile()){
				console.log('fileStreamRequest: path did not provide a file'); 
				return false;
			}


			var start = data.start || 0;
			var end = data.end || stat.size - 1;
			var info = { 
				chunkSize: end - start + 1,
				end: end,
				mime: VideoUtil.getExtMime(path), 
				size: stat.size,
				start: start
			}
			
			try {
				var stream = ss.createStream();	
				ss( app.socket ).emit( smEnum.fileStreamResponse, stream, info );
				fs.createReadStream( path , { flags: "r", start: start, end: end }).pipe(stream);
				
			}
			catch(err){
				console.log('VideoController.requestFileForPlayback: error creating read stream');
			}
		}
	});
});