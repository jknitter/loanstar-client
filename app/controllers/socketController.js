'use strict';

define([
	'underscore', 'socket.io-client', 'absorb', 'async', 'cli-color',

	'app/controllers/videoController',
	'app/controllers/contentController',

	'app/enums/socketMessageEnum'
],
function( _ , io, absorb, async, clc, VideoCtlr, ContentCtlr, smEnum){
	return function(app){
		function getMatchingFiles(config, callback){
			var fileList = [];

			_.each(app.contentCollections, function(cc){
				// if no term was passed just add the whole list
				if(!config.term){
					fileList = fileList.concat(Object.values(cc.fileList));
				}
				else {
					// use the flat list / object for easier searching
					_.each(cc.fileList, function(fileData){
						// find matches for the search term
						if(fileData.name.toLowerCase().indexOf(config.term.toLowerCase()) > -1){ 
							fileList.push(fileData);	
						}
					}, this);
				}
			}, this);
			
			callback(fileList);
		};

		function getMatchingCollections(config, callback){
			if(_.isFunction(config)){
				callback = config;
				config = {};
				console.log(callback + " should say function")
			}

			var list = [];
			if(!config.term) 
				list = app.contentCollections;
			else {
				_.each(app.contentCollections, function(cc){
					// find matches for the search term
					if(cc.name.toLowerCase().indexOf(config.term.toLowerCase()) > -1){ 
						list.push(cc);	
					}
				}, this);
			}
			callback(list);
		};

		function getUserData(config, callback){
			var fileCount = 0;
			_.each(app.contentCollections, function(cc){
				fileCount += Object.keys(cc.fileList || {}).length;
			}, this);

			callback({
				collectionCount: app.contentCollections.length,
				fileCount: fileCount,
				userName: app.user.userName
			});
		};

		//config = { name: collectionName }
		function onAddContentCollection(config){
			// update live data
			app.user.settings.contentCollections.push({name: config.name, paths:[]});
			app.contentCollections.push({name: config.name, contentItems: [], fileList: {}});
		};

		//config = { name: req.params.name, path: req.body.path, settings: {} }
		function onAddPathToContent(config, callback){
			var settingsContentCollection = _.findWhere(config.settings.contentCollections, { name: config.name });
			var path = _.findWhere(settingsContentCollection.paths, { path: config.path });

			// get the list of files 
			app.contentController.fetchFileList({name: config.name, paths: [path] }, function(err, results){
				var contentCollection = _.findWhere(app.contentCollections, { name: config.name });
				// update lists for the collection
				contentCollection.contentItems = contentCollection.contentItems.concat( results.contentItems );
				contentCollection.fileList = absorb(contentCollection.fileList, results.fileList, false, true); 

				if(callback) callback(null, config);
			});
		};

		//config = { name: collectionName }
		function onDeleteContentCollection(config, callback){
			// remove collection from content collections
			var collection = _.findWhere( app.contentCollections, {name: config.name} );
			var index = app.contentCollections.indexOf(collection);
			app.contentCollections.splice(index, 1);

			if(callback) callback();
		};

		// config = { name: collectionName, path: 'path\to\content' }
		function onDeletePathToContent(config, callback){
			var settingsContentCollection = _.findWhere(app.user.settings.contentCollections, {name: config.name});	
			// get the list of files 
			return app.contentController.fetchFileList({name: config.name, paths: [{path: config.oldPath}] }, function(err, results){
				var contentCollection = _.findWhere(app.contentCollections, {name: config.name});
	
				// update lists for the collection
				// remove from contentItems
				contentCollection.contentItems = _.filter(contentCollection.contentItems, function(contentItem){
					return !_.findWhere(results.contentItems, {uuid: contentItem.uuid})				
				});

				// remove from fileList
				_.each(results.fileList, function(item, key){
					delete contentCollection.fileList[key];
				});

				if(callback) callback(null, config);
			});
		};

 		// { name: collectionName, path: req.body.path, oldPath: oldPath }
		function onEditPathToContent(config){
			async.waterfall([
				async.apply(onDeletePathToContent, config), 
				onAddPathToContent
			], 
			function(err, results){
				if(err)
					console.log('onEditPathToContent errored')
			});
		};

		function peerDisconnected(data){
			//TODO - remove peer from our users list and tell the browser
			console.log(clc.cyan('User ' + data + ' has disconnected'));
		};

		return {
			'init': function(callback){
				console.log(clc.yellow('Attempting to CONNECT to LoanStar '));

				var url = 'http://' + app.config.serverInfo.hostName;
				if(app.config.serverInfo.hostPort) 
					url += ':' + app.config.serverInfo.hostPort;
				
				// socket to main server	
				var socket = app.socket = io.connect(url, {'forceNew': true});
				
				socket.on( smEnum.connectionComplete, function(){
					console.log(clc.green('Connected to LoanStar'));
					console.log('')
					callback();
				});		

				socket.on( smEnum.peerDisconnected, peerDisconnected);

				// Search
				socket.on(smEnum.Search_GetMatchingFiles, getMatchingFiles);
				socket.on(smEnum.Search_GetMatchingCollections, getMatchingCollections);
				
				// Neighbors
				socket.on(smEnum.Neighbors_GetUserData, getUserData);
				
				// Settings
				socket.on(smEnum.Settings_AddContentCollection, onAddContentCollection);
				socket.on(smEnum.Settings_AddPathToContent, onAddPathToContent);
				socket.on(smEnum.Settings_DeleteContentCollection, onDeleteContentCollection);
				socket.on(smEnum.Settings_DeletePathToContent, onDeletePathToContent);
				socket.on(smEnum.Settings_EditPathToContent, onEditPathToContent)
				
				// User
				socket.on(smEnum.User_GetUserCollections, getMatchingCollections);
				
				// Video
				socket.on( smEnum.fileStreamRequest, VideoCtlr.requestFileForPlayback.bind(VideoCtlr, app));
				
				socket.on('error', function(err){ console.log(clc.redBright(err)); });
				socket.on(smEnum.disconnect, function(){
					console.log(clc.redBright('Oh no, looks like the server went down. Exiting LoanStar Client-Server'))
					process.exit(1);
				})
			}
		} 
	}
});