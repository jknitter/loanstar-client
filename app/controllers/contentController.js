'use strict';

define([
	'absorb', 'underscore', 'fs', 'path', 'utils/js/crypto'
],
function(absorb, _ , fs, path, CryptoUtil){
	var insp = require('util').inspect;
	var acceptableExtenisons = ['mp4', 'm4v', 'mkv'];

	function ContentController(userName){
		this.userName = userName;
		return this;
	}
	
	ContentController.prototype.joinResults = function(allResults, newResults){
		allResults.contentItems = allResults.contentItems.concat(newResults.contentItems || []);					
		allResults.fileList = absorb(allResults.fileList, newResults.fileList || {}, false, true);							
	}
	
	ContentController.prototype.getFileData = function(fileName, newFilePath, collectionName){
		return { 
			'name': fileName, 
			'uuid': CryptoUtil.encrypt(newFilePath), 
			'path': newFilePath,
			'userName': this.userName || '',
			'collectionName': collectionName
		};
	};

	// build contentCollections array for the ui, 
	// passed an array of settings.contentCollections, which contains the paths to the media
	ContentController.prototype.init = function(collections, callback){
		var pending = _.size(collections);
		var contentCollections = [];
		// if there are no collections
		if(!pending) callback(null, contentCollections);
		// fetch the file list for each collection
		for(var i = 0; i < collections.length; i++){
			// fetchFileList gets called with this as content controller, 
			// in the callback, this as the collection, to access "name"
			this.fetchFileList(collections[i], function(err, results){
				if(err) return callback(err);

				contentCollections.push({ name: this.name, contentItems: results.contentItems, fileList: results.fileList });
			
				if(!--pending)
					return callback(null, contentCollections); 

			}.bind(collections[i]));
		}	
	};

	ContentController.prototype.fetchFileList = function(collection, callback){
		var paths = collection.paths;
		// if no paths, just return
		if(!paths || paths.length === 0) return callback(null, []);

		var uris = _.map(paths, function(item){
			return item.path;
		});

		var pending = uris.length;
		var returnFiles = { 'contentItems': [], 'fileList': {} };
		
		_.each(uris, function (uri) {
			// get the absolute path to this dir
			uri = path.resolve(uri);
			// don't walk if the dir doesn't exist
			fs.stat(uri, function(err, stat){
			    if(err){
			    	// we dont do anything if it errors, just check the callback
			    	if(!--pending) callback(null, returnFiles); 
			    	return; 
			    }
			    else if(stat.isFile()){
			    	var fileName = uri.substr(_.lastIndexOf(uri,'\\')+1);
			    	
			    	// if no . in the file name
			    	if(fileName.lastIndexOf('.') === -1){
			    		if(!--pending) callback(null, returnFiles); 
			    		return;
			    	}
			    	else {
						var fileData = this.getFileData(fileName, uri, collection.name);

						this.processContentObject(fileData, function(err, processResults){
							this.joinResults(returnFiles, processResults);
	
							// if all of the uri have been processed 
							if(!--pending) 
								return callback(null, returnFiles);
						}.bind(this));
			    	}		    		
			    }
			    else {
					// make this in walkContentDir conentController
					this.walkContentDir(uri, collection.name, function(err, results){
						if(err){
							console.log('fetchFileList walk callback errored, err = ' + insp(err));
						    throw new Error(err);
						}

						this.joinResults(returnFiles, results);
						
						// if all of the uri have been processed 
						if(!--pending) 
							return callback(null, returnFiles);
					}.bind(this));
				}
			}.bind(this));
		}, this);
	};

	// used to handle process one file/dir at a time	
	ContentController.prototype.processContentObject = function(fileData, done){
		var returnFiles = {contentItems: [], fileList: {}};
		// get the stats on the file
		fs.stat(fileData.path, function(err, stat){
			if(err || !stat){
				console.log('processContentObject.readdir.stat errored, err = ' + insp(err));
				return done(err);		
			}
			
			// if the object is a directory
			if(stat.isDirectory()){
				fileData.isDir = true;
				// get child dirs/files
				this.walkContentDir(fileData.path, fileData.collectionName, function(err, walkResults){
					// the path for this entry is no longer needed
					delete fileData.path;

					if( _.size(walkResults.contentItems) > 0)
						this.joinResults(returnFiles, walkResults);
					
					return done(null, returnFiles);
				}.bind(this));
			}
			// if the object is a file.
			else {
				// take the extension off of the file name
				var parts = fileData.name.split('.');
				var ext = parts.pop();

				// then put ir back together w/o the extension
				fileData.name = parts.join('.');
				fileData.ext = ext;
				
				if(acceptableExtenisons.indexOf(ext.toLowerCase()) > -1){
					delete fileData.path;
					// normal content/file structure
					returnFiles.contentItems.push(fileData);
					// flat list of files by id
					returnFiles.fileList[fileData.name] = fileData;
				}
				
				return done(null, returnFiles);
			}
		}.bind(this));
	};

	/*	@desc  walk a path and build out the list of files/directories
			files and dirs both get uuids

		@param dir {string} - the path to walk on this computer
		@param done {function} - the callback for when everything is done

		@return example - 
		[{
			name: "",
			uuid: "",
			path: ""

			if dir - 
			isDir: bool,
			contentItems: [
				{...},
				{...},
				...
			]
		}]
	*/
	ContentController.prototype.walkContentDir = function(dir, collectionName, done){		
		var returnFiles = {contentItems: [], fileList: {}};
		
		fs.readdir(dir, function(err, filesDirsFound){
			if(err){
				console.log('walkContentDir.readdir errored, err = ' + insp(err));
				return done(err);		
			}
			// how many objects in this dir?
			var pending = filesDirsFound.length;
			// if none
			if(pending === 0) return done(null, returnFiles);
			// otherwise
			_.each(filesDirsFound, function(fileName){
				var newFilePath = dir + '/' + fileName;

				var fileData = this.getFileData(fileName, newFilePath, collectionName);
				
				this.processContentObject(fileData, function(err, processResults){
					this.joinResults(returnFiles, processResults);

					if(--pending === 0) 
						return done(null, returnFiles);			
				}.bind(this));
			}, this);
		}.bind(this));
	};
	//*************************************************************//


	//**************** content collections search utils *****************//
	// traverses the hierarchy from passed contetnItem looking for a contentItem that has the passed id
	ContentController.prototype.findContentItemById = function(contentItem, id){
		//  if the passed fileOrDir has the id we are looking for
		if(contentItem.uuid == id) return contentItem;
		// if the contentItem has contentItems to check
		else if(contentItem.contentItems) {
			for(var i = 0; i < contentItem.contentItems.length; i++){
				var recurseResult = this.findContentItemById( contentItem.contentItems[i], id );
				if(recurseResult) return recurseResult;
			}
		}
	};

	// finds the content item by uuid in the passed collection
	ContentController.prototype.loacateContentItem = function(collection, uuid){
		var fileData;
	
		// get the list of content items in the collection, and find the file. recursive dive
		var items = collection.contentItems;
		for(var i = 0; i < items.length; i++){
			fileData = this.findContentItemById( items[i], uuid );
			if(fileData) break;
		}
	
		return fileData;
	}

	return ContentController;
});