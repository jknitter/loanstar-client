'use strict';

define(function(){
	var base = {
		title: 'Client-Server', 
		description: 'Stream Loaning, file loaning',
		keywords: 'streaming, movies, music, p2p, loan, loanstar',
		serverInfo: {
			hostName: 'loanstar.jacobknitter.com'
			//hostName: 'localhost:4001'
		},
		port: 4000
	};

	return base;
});