'use strict';

var requirejs = require('requirejs');
requirejs.config({
	baseUrl: '.',
	nodeRequire: require,
	waitSeconds: 0,

	// please keep in abc order
	paths: {
		'config': 'app/config/env/base',
		'globber': 'utils/file/globber'
	}
});

module.exports = requirejs;
