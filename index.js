'use strict';
	
var requirejs = require('./app/config/init/requireInit');
requirejs([
	'prompt', 'underscore', 'http', 'cli-color',
	
	'./app/controllers/contentController', './app/controllers/socketController', './app/controllers/userController',
	'./utils/js/crypto', 'config', 'app/enums/socketMessageEnum'
],
function(prompt, _ , http, clc, ContentController, SocketController, UserController, CryptoUtil, config, smEnum){
	var prompting = false;
	function promptForLogin(){
		if(prompting) return;

		console.log('--- Sign in ---');
		// Start the prompt 
		prompt.start();
		prompting = true;

		var schema = {
		    properties: {
			    userName: {
			        pattern: /^[\w]{4,}$/,
			        message: 'Name must be 4 or more letters or numbers',
			        required: true
			    },
			    password: {
			        hidden: true,
			        required: true,
			    //    pattern: /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/,
			    //    message: 'Passwords are 8 or more letters or numbers, and contain at least one uppercase, lowercase, and numberic character'
			    }
			}
		};

		// Get two properties from the user: username and password 
		prompt.get(schema, function (err, result) {
			prompting = false;
			var user = { 
				userName: result.userName.trim(),
				password: CryptoUtil.encrypt(result.password) 
			};

			if(result.userName && result.password){
				UserController.logIntoServer(app, user, function(err, user){
					if(err || !user){
						// blank line
						console.log('');
						promptForLogin();
					}
					else {
						app.user = user;
						init(user);
					}
				});
			}
		});
	};

	function init(user){
		if(user){
		 	app.contentController = new ContentController(app.user.userName);
			console.log('')
			console.log(clc.yellow('Gathering your conent'));

			// get the list of content this app will make available
			app.contentController.init( user.settings.contentCollections, function(err, collectionsData){				
				console.log(clc.green('Done Gathering'));
				
				app.contentCollections = collectionsData;
				
				var port = process.env.PORT || user.settings.port || app.config.port || 4000;
				app.listen(port, function() { 
					console.log('')
					console.log(clc.greenBright('Client-Server started on port: ') + clc.cyanBright(port));

					app.socket.emit(smEnum.clientInitComplete, { userName: app.user.userName });
				});
			});
		}
	};

	var app = http.createServer();
	app.config = config;
	SocketController(app).init(promptForLogin);	
});