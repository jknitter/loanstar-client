var	fs = require('fs'),
	gulp = require('gulp'),
    uglify = require('gulp-uglify');

var basePath = '../';

//**** main tasks to bundle the app ****//
gulp.task('prod', function(){
	// copy all under app, keep data folder too, but no data
	gulp.src(['app/**/*'], { cwd: basePath })
	.pipe(uglify())//.on('error', function(err){ console.log(err); })
	.pipe(gulp.dest(basePath+'dist/app'));

	gulp.src(['index.js'], {cwd: basePath })
	.pipe(uglify())
	
	.pipe(gulp.dest(basePath+'dist'));

	// copy all package files
	gulp.src(['package.json',], {cwd: basePath })
	.pipe(gulp.dest(basePath+'dist'));

	gulp.src(['utils/**/*'], {cwd: basePath })
	.pipe(uglify())
	.pipe(gulp.dest(basePath+'dist/utils'));
});

//**** end main tasks to bundle the app ****//
